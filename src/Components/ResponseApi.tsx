
interface Props{
    apiResponse: string;
}

export function ResponseApi(props :Props){
    return (
        <div style={{ color: 'white' }}>{props.apiResponse}</div>
    );
}

export default ResponseApi;