import React, { ChangeEvent } from 'react';
import { Component } from 'react';

interface InputProps {
  value: string;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
}

export class SendRequestInput extends Component<InputProps>{
  render (){
        return (
        <input
        type="text"
        value={this.props.value}
        onChange={this.props.onChange}
        placeholder="Введите URL API"
        />
    );
    }
};

export default SendRequestInput;



// import React, { ChangeEvent } from 'react';

// interface InputProps {
//   value: string;
//   onChange: (event: ChangeEvent<HTMLInputElement>) => void;
// }

// const SendRequestInput: React.FC<InputProps> = (props) => {
//   return (
//     <input
//       type="text"
//       value={props.value}
//       onChange={props.onChange}
//       placeholder="Введите URL API"
//     />
//   );
// };

// export default SendRequestInput;