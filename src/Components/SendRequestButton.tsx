
interface Props{
    onClick: () => void;
    text: string;
}

export function SendRequestButton(props: Props){
    return (
            <button onClick={props.onClick}>{props.text}</button>
          );
}

export default SendRequestButton;