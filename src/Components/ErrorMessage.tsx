import '../App.css';

interface Props{
    Message: string;
}

export function ErrorMessage(props :Props){
    return (
        <div className='ErrorMessageText'>
            {props.Message}
        </div>
    );
}

export default ErrorMessage;