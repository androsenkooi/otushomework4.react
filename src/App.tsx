import React, {useState} from 'react';
import './App.css'
import axios from 'axios';
import SendRequestButton from './Components/SendRequestButton';
import SendRequestInput from './Components/SendRequestInput';
import ErrorMessage from './Components/ErrorMessage';
import ResponseApi from './Components/ResponseApi';

const App: React.FC =() => {
  const [url, setUrl] = useState<string>('');
  const [apiResponse, setApiResponse] = useState<string>('');
  const [errorMessage, setErrorMessage] = useState<string>('');

    const handleSubmit = async () => {
      ClearMessages();
      try {
        if(url.length == 0)        
          setErrorMessage('Введите URL API')
        
        else{
          const response = await axios.get(url);
          setApiResponse(JSON.stringify(response.data));
        }
      } catch (error) {
        setErrorMessage('Ошибка при вызове API');
      }
    };

    return  <>
    <div className="App-header">
        <SendRequestInput value={url} onChange={(e) => setUrl(e.target.value)} />
        <SendRequestButton onClick={handleSubmit} text="Отправить" />
        <ResponseApi apiResponse={apiResponse}/>
        <ErrorMessage Message={errorMessage} />
      </div>
    </>;

  function ClearMessages(){
    setApiResponse('');
    setErrorMessage('');
  }
}



export default App;
